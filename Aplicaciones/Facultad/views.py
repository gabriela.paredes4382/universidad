from django.shortcuts import render, redirect
from .models import CarreraGEPT
from .models import CursoGEPT
from .models import AsignaturaGEPT
from django.contrib import messages
# CARRERA
def listadoCarreras(request):
    carrerasBdd=CarreraGEPT.objects.all()
    return render(request, 'listadoCarreras.html',{'carreras':carrerasBdd})

def guardarCarrera(request):
    nombreCarreraGEPT=request.POST["nombreCarreraGEPT"]
    directorCarreraGEPT=request.POST["directorCarreraGEPT"]
    logoCarreraGEPT=request.FILES.get("logoCarreraGEPT")
    facultadCarreraGEPT=request.POST["facultadCarreraGEPT"]
    modalidadCarreraGEPT=request.POST["modalidadCarreraGEPT"]
    #Insertando datos mediante el ORM de DJango
    nuevoCarrera=CarreraGEPT.objects.create(nombreCarreraGEPT=nombreCarreraGEPT,directorCarreraGEPT=directorCarreraGEPT,logoCarreraGEPT=logoCarreraGEPT,facultadCarreraGEPT=facultadCarreraGEPT,modalidadCarreraGEPT=modalidadCarreraGEPT)
    messages.success(request,'Carrera Guardada Exitosamente')
    return redirect('/')
def eliminarCarrera(request,idCarreraGEPT):
    carreraEliminar=CarreraGEPT.objects.get(idCarreraGEPT=idCarreraGEPT)
    carreraEliminar.delete()
    messages.error(request,'Carrera Eliminada Exitosamente')
    return redirect('/')
def editarCarrera(request,idCarreraGEPT):
    carreraEditar=CarreraGEPT.objects.get(idCarreraGEPT=idCarreraGEPT)
    return render(request,'editarCarrera.html',{'carrera':carreraEditar})
def procesarActualizacionCarrera(request):
    idCarreraGEPT=request.POST["idCarreraGEPT"]
    nombreCarreraGEPT=request.POST["nombreCarreraGEPT"]
    directorCarreraGEPT=request.POST["directorCarreraGEPT"]
    logoCarreraGEPT=request.FILES.get("logoCarreraGEPT")
    facultadCarreraGEPT=request.POST["facultadCarreraGEPT"]
    modalidadCarreraGEPT=request.POST["modalidadCarreraGEPT"]
    #Insertando datos mediante el ORM de DJANGO
    carreraEditar=CarreraGEPT.objects.get(idCarreraGEPT=idCarreraGEPT)
    carreraEditar.nombreCarreraGEPT=nombreCarreraGEPT
    carreraEditar.directorCarreraGEPT=directorCarreraGEPT
    # Verificar si se proporcionó una nueva imagen
    if logoCarreraGEPT:
        carreraEditar.logoCarreraGEPT = logoCarreraGEPT
    carreraEditar.facultadCarreraGEPT=facultadCarreraGEPT
    carreraEditar.modalidadCarreraGEPT=modalidadCarreraGEPT
    carreraEditar.save()
    messages.warning(request,
      'Carrera Actualizada Exitosamente')
    return redirect('/')

#CURSO

def listadoCursos(request):
    cursosBdd=CursoGEPT.objects.all()
    carrerasBdd=CarreraGEPT.objects.all()
    return render(request, 'listadoCursos.html',{'cursos':cursosBdd,'carreras':carrerasBdd})

def guardarCurso(request):
    #campurando los valores del formulario por POST
    id_carrera=request.POST["id_carrera"]
    #campurando el tipo seleccionado por el usuario
    carreraSeleccionado=CarreraGEPT.objects.get(idCarreraGEPT=id_carrera)
    nivelCursoGEPT=request.POST["nivelCursoGEPT"]
    descripcionCursoGEPT=request.POST["descripcionCursoGEPT"]
    aulaCursoGEPT=request.POST["aulaCursoGEPT"]
    bloqueCursoGEPT=request.POST["bloqueCursoGEPT"]
    horarioCursoGEPT=request.POST["horarioCursoGEPT"]
    #Insertando datos mediante el ORM de DJango
    nuevoCurso=CursoGEPT.objects.create(nivelCursoGEPT=nivelCursoGEPT,descripcionCursoGEPT=descripcionCursoGEPT,aulaCursoGEPT=aulaCursoGEPT,bloqueCursoGEPT=bloqueCursoGEPT,horarioCursoGEPT=horarioCursoGEPT,carrera=carreraSeleccionado)
    messages.success(request,'Curso Guardado Exitosamente')
    return redirect('listadoCursos')
def eliminarCurso(request, idCursoGEPT):
    cursoEliminar=CursoGEPT.objects.get(idCursoGEPT=idCursoGEPT)
    cursoEliminar.delete()
    messages.error(request,'Curso Eliminado Exitosamente')
    return redirect('listadoCursos')
def editarCurso(request,idCursoGEPT):
    cursoEditar=CursoGEPT.objects.get(idCursoGEPT=idCursoGEPT)
    carrerasBdd=CarreraGEPT.objects.all()
    return render(request,'editarCurso.html',{'curso':cursoEditar,'carreras':carrerasBdd})
def procesarActualizacionCurso(request):
    idCursoGEPT=request.POST["idCursoGEPT"]
    id_carrera=request.POST["id_carrera"]
    carreraSeleccionado=CarreraGEPT.objects.get(idCarreraGEPT=id_carrera)
    nivelCursoGEPT=request.POST["nivelCursoGEPT"]
    descripcionCursoGEPT=request.POST["descripcionCursoGEPT"]
    aulaCursoGEPT=request.POST["aulaCursoGEPT"]
    bloqueCursoGEPT=request.POST["bloqueCursoGEPT"]
    horarioCursoGEPT=request.POST["horarioCursoGEPT"]
    #Insertando datos mediante el ORM de DJANGO
    cursoEditar=CursoGEPT.objects.get(idCursoGEPT=idCursoGEPT)
    cursoEditar.carrera=carreraSeleccionado
    cursoEditar.nivelCursoGEPT=nivelCursoGEPT
    cursoEditar.descripcionCursoGEPT=descripcionCursoGEPT
    cursoEditar.aulaCursoGEPT=aulaCursoGEPT
    cursoEditar.bloqueCursoGEPT=bloqueCursoGEPT
    cursoEditar.horarioCursoGEPT=horarioCursoGEPT
    cursoEditar.save()
    messages.warning(request,
      'Curso Actualizado Exitosamente')
    return redirect('listadoCursos')

#ASIGNATURA

def listadoAsignaturas(request):
    asignaturasBdd=AsignaturaGEPT.objects.all()
    cursosBdd=CursoGEPT.objects.all()
    return render(request, 'listadoAsignaturas.html',{'asignaturas':asignaturasBdd,'cursos':cursosBdd})

def guardarAsignatura(request):
    #campurando los valores del formulario por POST
    id_curso=request.POST["id_curso"]
    #campurando el tipo seleccionado por el usuario
    cursoSeleccionado=CursoGEPT.objects.get(idCursoGEPT=id_curso)
    nombreAsignaturaGEPT=request.POST["nombreAsignaturaGEPT"]
    creditosAsignaturaGEPT=request.POST["creditosAsignaturaGEPT"]
    fechaInicioAsignaturaGEPT=request.POST["fechaInicioAsignaturaGEPT"]
    fechaFinalizacionAsignaturaGEPT=request.POST["fechaFinalizacionAsignaturaGEPT"]
    profesorAsignaturaGEPT=request.POST["profesorAsignaturaGEPT"]
    silaboAsignaturaGEPT=request.FILES.get("silaboAsignaturaGEPT")
    horaSemanalAsignaturaGEPT=request.POST["horaSemanalAsignaturaGEPT"]
    semestreAsignaturaGEPT=request.POST["semestreAsignaturaGEPT"]
    #Insertando datos mediante el ORM de DJango
    nuevoAsignatura=AsignaturaGEPT.objects.create(nombreAsignaturaGEPT=nombreAsignaturaGEPT,creditosAsignaturaGEPT=creditosAsignaturaGEPT,fechaInicioAsignaturaGEPT=fechaInicioAsignaturaGEPT,fechaFinalizacionAsignaturaGEPT=fechaFinalizacionAsignaturaGEPT,profesorAsignaturaGEPT=profesorAsignaturaGEPT,silaboAsignaturaGEPT=silaboAsignaturaGEPT,horaSemanalAsignaturaGEPT=horaSemanalAsignaturaGEPT,semestreAsignaturaGEPT=semestreAsignaturaGEPT,curso=cursoSeleccionado)
    messages.success(request,'Asignatura Guardada Exitosamente')
    return redirect('listadoAsignaturas')
def eliminarAsignatura(request,idAsignaturaGEPT):
    asignaturaEliminar=AsignaturaGEPT.objects.get(idAsignaturaGEPT=idAsignaturaGEPT)
    asignaturaEliminar.delete()
    messages.error(request,'Asignatura Eliminada Exitosamente')
    return redirect('listadoAsignaturas')
def editarAsignatura(request,idAsignaturaGEPT):
    asignaturaEditar=AsignaturaGEPT.objects.get(idAsignaturaGEPT=idAsignaturaGEPT)
    cursosBdd=CursoGEPT.objects.all()
    return render(request,'editarAsignatura.html',{'asignatura':asignaturaEditar,'cursos':cursosBdd})
def procesarActualizacionAsignatura(request):
    idAsignaturaGEPT=request.POST["idAsignaturaGEPT"]
    id_curso=request.POST["id_curso"]
    cursoSeleccionado=CursoGEPT.objects.get(idCursoGEPT=id_curso)
    nombreAsignaturaGEPT=request.POST["nombreAsignaturaGEPT"]
    creditosAsignaturaGEPT=request.POST["creditosAsignaturaGEPT"]
    fechaInicioAsignaturaGEPT=request.POST["fechaInicioAsignaturaGEPT"]
    fechaFinalizacionAsignaturaGEPT=request.POST["fechaFinalizacionAsignaturaGEPT"]
    profesorAsignaturaGEPT=request.POST["profesorAsignaturaGEPT"]
    silaboAsignaturaGEPT=request.FILES.get("silaboAsignaturaGEPT")
    horaSemanalAsignaturaGEPT=request.POST["horaSemanalAsignaturaGEPT"]
    semestreAsignaturaGEPT=request.POST["semestreAsignaturaGEPT"]
    #Insertando datos mediante el ORM de DJANGO
    asignaturaEditar=AsignaturaGEPT.objects.get(idAsignaturaGEPT=idAsignaturaGEPT)
    asignaturaEditar.curso=cursoSeleccionado
    asignaturaEditar.nombreAsignaturaGEPT=nombreAsignaturaGEPT
    asignaturaEditar.creditosAsignaturaGEPT=creditosAsignaturaGEPT
    asignaturaEditar.fechaInicioAsignaturaGEPT=fechaInicioAsignaturaGEPT
    asignaturaEditar.fechaFinalizacionAsignaturaGEPT=fechaFinalizacionAsignaturaGEPT
    asignaturaEditar.profesorAsignaturaGEPT=profesorAsignaturaGEPT
    # Verificar si se proporcionó un nuevo silabo
    if silaboAsignaturaGEPT:
        asignaturaEditar.silaboAsignaturaGEPT = silaboAsignaturaGEPT
    asignaturaEditar.horaSemanalAsignaturaGEPT=horaSemanalAsignaturaGEPT
    asignaturaEditar.semestreAsignaturaGEPT=semestreAsignaturaGEPT
    asignaturaEditar.save()
    messages.warning(request,
      'Asignatura Actualizada Exitosamente')
    return redirect('listadoAsignaturas')
def visita1(request):
    return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)

        messages.success(request, 'Se ha enviado tu Correo')
        return HttpResponseRedirect('/visita1')

    return render(request, 'enviar_correo.html')
