from django.urls import path
from . import views

urlpatterns=[
    #CARRERA
    path('',views.listadoCarreras),
    path('guardarCarrera/',views.guardarCarrera),
    path('eliminarCarrera/<idCarreraGEPT>',views.eliminarCarrera),
    path('editarCarrera/<idCarreraGEPT>',views.editarCarrera),
    path('procesarActualizacionCarrera/',views.procesarActualizacionCarrera),
    #CURSOS
    path('listadoCursos/',views.listadoCursos, name='listadoCursos'),
    path('guardarCurso/',views.guardarCurso),
    path('eliminarCurso/<idCursoGEPT>',views.eliminarCurso),
    path('editarCurso/<idCursoGEPT>',views.editarCurso),
    path('procesarActualizacionCurso/',views.procesarActualizacionCurso),
    #ASIGNATURAS
    path('listadoAsignaturas/',views.listadoAsignaturas, name='listadoAsignaturas'),
    path('guardarAsignatura/',views.guardarAsignatura),
    path('eliminarAsignatura/<idAsignaturaGEPT>',views.eliminarAsignatura),
    path('editarAsignatura/<idAsignaturaGEPT>',views.editarAsignatura),
    path('procesarActualizacionAsignatura/',views.procesarActualizacionAsignatura),
    path('enviar-correo/',views.enviar_correo, name='enviar_correo'),
    path('visita1/',views.visita1, name='visita1')
]
