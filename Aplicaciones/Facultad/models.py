from django.db import models

# Create your models here.
class CarreraGEPT(models.Model):
    idCarreraGEPT=models.AutoField(primary_key=True)
    nombreCarreraGEPT=models.CharField(max_length=150)
    directorCarreraGEPT=models.CharField(max_length=150)
    logoCarreraGEPT=models.FileField(upload_to='carreras', null=True, blank=True)
    facultadCarreraGEPT=models.CharField(max_length=150)
    modalidadCarreraGEPT=models.CharField(max_length=150)
    def __str__(self):
        fila="{0}: {1} {2} {3} {4} {5}"
        return fila.format(self.idCarreraGEPT, self.nombreCarreraGEPT, self.directorCarreraGEPT, self.logoCarreraGEPT, self.facultadCarreraGEPT, self.modalidadCarreraGEPT)

class CursoGEPT(models.Model):
    idCursoGEPT=models.AutoField(primary_key=True) #AutoField es como un autoincrementable
    nivelCursoGEPT=models.CharField(max_length=150) #es un varchar
    descripcionCursoGEPT=models.TextField() #es un varchar
    aulaCursoGEPT=models.CharField(max_length=150) #es un varchar
    bloqueCursoGEPT=models.CharField(max_length=150) #es un texto sin definir el espacio
    horarioCursoGEPT=models.CharField(max_length=150)  #es una fecha
    carrera=models.ForeignKey(CarreraGEPT, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        fila="{0}: {1} {2} {3} {4} {5}"
        return fila.format(self.idCursoGEPT, self.nivelCursoGEPT, self.descripcionCursoGEPT, self.aulaCursoGEPT, self.bloqueCursoGEPT, self.horarioCursoGEPT)

class AsignaturaGEPT(models.Model):
    idAsignaturaGEPT=models.AutoField(primary_key=True) #AutoField es como un autoincrementable
    nombreAsignaturaGEPT=models.CharField(max_length=150) #es un varchar
    creditosAsignaturaGEPT=models.CharField(max_length=150)  #es un varchar
    fechaInicioAsignaturaGEPT=models.DateField() #es un varchar
    fechaFinalizacionAsignaturaGEPT=models.DateField()
    profesorAsignaturaGEPT=models.CharField(max_length=150) #es un texto sin definir el espacio
    silaboAsignaturaGEPT=models.FileField(upload_to='asignaturas', null=True, blank=True)
    horaSemanalAsignaturaGEPT=models.CharField(max_length=150)
    semestreAsignaturaGEPT=models.CharField(max_length=150)
    curso=models.ForeignKey(CursoGEPT, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        fila="{0}: {1} {2} {3} {4} {5} {6} {7} {8}"
        return fila.format(self.idAsignaturaGEPT, self.nombreAsignaturaGEPT, self.creditosAsignaturaGEPT, self.fechaInicioAsignaturaGEPT, self.fechaFinalizacionAsignaturaGEPT, self.profesorAsignaturaGEPT, self.silaboAsignaturaGEPT, self.horaSemanalAsignaturaGEPT, self.semestreAsignaturaGEPT)
