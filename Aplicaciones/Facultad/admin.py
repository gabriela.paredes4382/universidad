from django.contrib import admin
from .models import CarreraGEPT
from .models import CursoGEPT
from .models import AsignaturaGEPT
# Register your models here.
#admin.site.register(CarreraGEPT)
#admin.site.register(CursoGEPT)
#admin.site.register(AsignaturaGEPT)
@admin.register(CarreraGEPT)
class CarreraAdmin(admin.ModelAdmin):
    list_display = ('idCarreraGEPT', 'nombreCarreraGEPT')
    list_display_links = ('nombreCarreraGEPT',)
    search_fields = ('nombreCarreraGEPT',)
    list_per_page = 2
    list_filter = ('nombreCarreraGEPT',)

@admin.register(CursoGEPT)
class CursoAdmin(admin.ModelAdmin):
    list_display = ('idCursoGEPT', 'nivelCursoGEPT', 'aulaCursoGEPT')
    list_display_links = ('nivelCursoGEPT', 'aulaCursoGEPT',)
    search_fields = ('nivelCursoGEPT',)
    list_per_page = 2
    list_filter = ('nivelCursoGEPT',)

@admin.register(AsignaturaGEPT)
class AsignaturaAdmin(admin.ModelAdmin):
    list_display = ('idAsignaturaGEPT', 'nombreAsignaturaGEPT', 'creditosAsignaturaGEPT')
    list_display_links = ('nombreAsignaturaGEPT', 'creditosAsignaturaGEPT',)
    search_fields = ('nombreAsignaturaGEPT',)
    list_per_page = 2
    list_filter = ('nombreAsignaturaGEPT',)
